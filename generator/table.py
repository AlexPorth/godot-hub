import logging
import re
from datetime import datetime

import requests
from bs4 import BeautifulSoup, element

from data_types.godot_downloadable import GodotDownloadable

from data_types.link_type import LinkType
from generator.json_file import JsonFile


class Table:
    def __init__(self):
        self.url = None
        self.name = "root"
        self.rows: list[Row] = []

    def is_filled(self):
        return self.rows

    def read(self, url: str):
        self.url = url
        raw_html = self.get_html()
        self.parse_table(html=raw_html)

    def get_html(self):
        url_result = requests.get(self.url)
        if url_result.status_code != 200:
            exit(1)
        return url_result.text

    def parse_table(self, html: str):
        soup_html = BeautifulSoup(html, "lxml")
        soup_table = soup_html.find("table", summary="Directory Listing")
        soup_rows = soup_table.find_all("tr")
        for soup_row in soup_rows:
            row = Row(base_url=self.url, soup_row=soup_row)
            if not row.is_valid():
                continue
            row.parse()
            self.rows.append(row)
            if row.link_type & LinkType.APPLICATION:
                row.write_to_json()


class Row:
    datetime_regex = re.compile(r"\d{4}-[A-Z][a-z]{2}-\d{2} \d{2}:\d{2}:\d{2}")
    datetime_format = "%Y-%b-%d %H:%M:%S"
    unwanted_directories = ("Parent Directory", "fixup", "media", "patreon", "testing", "toolchains")

    def __init__(self, base_url: str, soup_row: element.Tag):
        self.base_url = base_url
        self._soup_row: element.Tag = soup_row
        self._soup_name_cell: element.Tag = soup_row.find("td", class_="n")
        self._soup_date_cell: element.Tag = soup_row.find("td", class_="m")
        self._soup_size_cell: element.Tag = soup_row.find("td", class_="s")
        self._soup_type_cell: element.Tag = soup_row.find("td", class_="t")
        self.name: str = ""
        self.url: str = ""
        self.last_modified: datetime = datetime.min
        self.link_type: LinkType = LinkType.NONE
        self.size: str = ""
        self.sub_table: Table = Table()

    def is_valid(self) -> bool:
        if self._soup_name_cell is None or self._soup_name_cell.a.text in self.unwanted_directories:
            return False
        return True

    def parse(self):
        if not self.is_valid():
            return
        self.parse_name()
        self.parse_last_modified()
        self.parse_type()
        if self.link_type is LinkType.DIRECTORY:
            self.parse_sub_table()
        else:
            self.parse_size()
        logging.info(f"Parsed Row for: {self.url}")

    def parse_name(self):
        self.name = self._soup_name_cell.a.text
        self.url = f"{self.base_url}{self._soup_name_cell.a.get('href')}"

    def parse_last_modified(self):
        datetime_string = self._soup_date_cell.text
        if self.datetime_regex.match(datetime_string) is None:
            return
        self.last_modified = datetime.strptime(datetime_string, self.datetime_format)

    def parse_type(self):
        match self._soup_type_cell.text:
            case "text/plain":
                self.link_type = LinkType.TEXT
            case "Directory":
                self.link_type = LinkType.DIRECTORY
            case "application/zip":
                self.link_type = LinkType.APPLICATION_ZIP
            case "application/x-xz":
                self.link_type = LinkType.APPLICATION_X_XZ
            case "application/vnd.android.package-archive":
                self.link_type = LinkType.APPLICATION_ANDROID
            case "application/octet-stream":
                self.link_type = LinkType.APPLICATION_OCTET_STREAM
            case _:
                logging.error(f"Found type has no LinkType entry for: {self._soup_type_cell.text}")

    def parse_size(self):
        self.size = self._soup_size_cell.text

    def parse_sub_table(self):
        self.sub_table.read(url=self.url)

    def write_to_json(self):
        application_entry = GodotDownloadable()
        application_entry.process_data(row=self)
        JsonFile.write_entry(entry=application_entry.dict())
