import json
from pathlib import Path


class JsonFile:
    file_path = None
    file_name: Path = Path("godot-applications.json")
    file_content = []

    @classmethod
    def write_entry(cls, entry: dict):
        cls.file_content.append(entry)

    @classmethod
    def save(cls):
        with open(cls.file_path, "w") as file_handler:
            json.dump(cls.file_content, file_handler)

    @classmethod
    def reset(cls, base_file_path: Path):
        cls.file_content = []
        cls.file_path = base_file_path.joinpath(cls.file_name)
        open(cls.file_path, "w").close()
