import logging
from pathlib import Path

from generator.constants import GODOT_DOWNLOAD_BASE_URL
from generator.json_file import JsonFile
from generator.table import Table

logging.basicConfig(level=logging.INFO)

if __name__ == "__main__":
    file_path = Path("D:/Python/Projects/godot-hub")
    JsonFile.reset(file_path)
    table = Table()
    table.read(url=GODOT_DOWNLOAD_BASE_URL)
    JsonFile.save()
