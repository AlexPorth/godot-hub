from enum import IntFlag, auto


class LinkType(IntFlag):
    NONE = 0
    TEXT = auto()
    DIRECTORY = auto()
    APPLICATION = auto()
    ZIP = auto()
    X_XZ = auto()
    ANDROID = auto()
    OCTET_STREAM = auto()
    APPLICATION_ZIP = APPLICATION | ZIP
    APPLICATION_X_XZ = APPLICATION | X_XZ
    APPLICATION_ANDROID = APPLICATION | ANDROID
    APPLICATION_OCTET_STREAM = APPLICATION | OCTET_STREAM
