import re
from pathlib import Path
from urllib import parse

from data_types.binary import Binary
from data_types.os import OS
from generator.constants import GODOT_DOWNLOAD_BASE_URL


class GodotDownloadable:
    version_regex = re.compile(
        r"^(?P<major>\d+)\.(?P<minor>\d+)(\.(?P<patch>\d+))?(\.(?P<edit>\d+))?/"
        r"(?P<old_rc>rc/\d+/)?"
        r"(?P<pre_alpha>pre-alpha/\d+\.\d+-dev\.\d+/)?"
        r"(?P<alpha>alpha\d+/)?"
        r"(?P<beta>beta\d+/)?"
        r"(?P<rc>rc\d+/)?"
        r"(?P<mono>mono/)?"
    )
    strip_regex = re.compile(r"pre-alpha|\d+\.\d+-dev\.|alpha|beta|rc|/")
    os_regex = re.compile(
        r"(?P<os>win|osx|x11|linux_(?:headless|server)|web_editor|release|android_editor)[._]?"
        r"(?P<bit>\d+)?\.?(?P<binary>exe|fat|universal)?"
    )
    os_map = {
        "win": OS.WINDOWS,
        "x11": OS.X11,
        "osx": OS.MAC,
        "web_editor": OS.WEB,
        "linux_headless": OS.LINUX_HEADLESS,
        "linux_server": OS.LINUX_SERVER,
        "release": OS.ANDROID_LIBRARY,
        "android_editor": OS.ANDROID_APP,
    }
    binary_map = {
        "exe": Binary.EXE,
        "fat": Binary.FAT,
        "universal": Binary.UNIVERSAL,
    }
    non_os_regex = re.compile(r"(?:stable|(?:dev|alpha|beta|rc)[_.]?\d+)[_.](.*)$")

    def __init__(self):
        self._data = None
        self.major: int = 0
        self.minor: int = 0
        self.patch = None
        self.edit = None
        self.os: OS = OS.NONE
        self.bit = None
        self.binary = None
        self.mono: bool = False
        self.pre_alpha: bool = False
        self.pre_alpha_version = None
        self.alpha: bool = False
        self.alpha_version = None
        self.beta: bool = False
        self.beta_version = None
        self.release_candidate: bool = False
        self.release_candidate_version = None
        self.last_modified = None
        self.size = None
        self.url = None

        self.non_os_name = None

    def process_data(self, row):
        self._data = row
        self.url = row.url
        self.last_modified = row.last_modified
        self.size = row.size
        self.parse_for_version()
        self.parse_for_os()

    def parse_for_version(self):
        version_prep_string = self._data.base_url.replace(GODOT_DOWNLOAD_BASE_URL, "")
        version_dictionary = self.version_regex.match(version_prep_string).groupdict()
        self.major = int(version_dictionary["major"])
        self.minor = int(version_dictionary["minor"])
        patch = version_dictionary["patch"]
        if patch is not None:
            self.patch = int(patch)
        edit = version_dictionary["edit"]
        if edit is not None:
            self.edit = int(edit)
        self.release_candidate, self.release_candidate_version = self.check_development_level(
            development_entry=version_dictionary["old_rc"]
        )
        self.pre_alpha, self.pre_alpha_version = self.check_development_level(
            development_entry=version_dictionary["pre_alpha"]
        )
        self.alpha, self.alpha_version = self.check_development_level(development_entry=version_dictionary["alpha"])
        self.beta, self.beta_version = self.check_development_level(development_entry=version_dictionary["beta"])
        self.mono = version_dictionary["mono"] is not None

    def check_development_level(self, development_entry):
        if development_entry is not None:
            return True, int(self.strip_regex.sub("", development_entry))
        return False, None

    def parse_for_os(self):
        url_parts = parse.urlparse(self.url)
        url_path = Path(url_parts.path)
        os_match = self.os_regex.search(url_path.stem)
        if os_match is None:
            non_os_match = self.non_os_regex.search(url_path.stem)
            if non_os_match is None:
                return
            self.non_os_name = re.sub(r"[_.]", " ", non_os_match.group(1)).title()
            if ".xz" == url_path.suffix:
                self.non_os_name = "Source Code"
            if ".sha256" == url_path.suffix:
                self.non_os_name = "Source Code sha256"
            return
        os_dictionary = os_match.groupdict()
        self.os = self.os_map[os_dictionary["os"]]
        bit = os_dictionary["bit"]
        if bit is not None:
            self.bit = int(bit)
        binary = os_dictionary["binary"]
        if binary is not None:
            self.binary = self.binary_map[binary]
        return

    def dict(self) -> dict:
        return {
            "major": self.major,
            "minor": self.minor,
            "patch": self.patch,
            "edit": self.edit,
            "os": self.os,
            "bit": self.bit,
            "binary": self.binary,
            "mono": self.mono,
            "pre_alpha": self.pre_alpha,
            "pre_alpha_version": self.pre_alpha_version,
            "alpha": self.alpha,
            "alpha_version": self.alpha_version,
            "beta": self.beta,
            "beta_version": self.beta_version,
            "release_candidate": self.release_candidate,
            "release_candidate_version": self.release_candidate_version,
            "last_modified": None if self.last_modified is None else self.last_modified.strftime("%Y-%m-%d %H:%M:%S"),
            "size": self.size,
            "url": self.url,
            "non_os_name": self.non_os_name,
        }
