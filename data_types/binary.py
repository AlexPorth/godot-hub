from enum import IntFlag, auto


class Binary(IntFlag):
    NONE = 0
    EXE = auto()
    FAT = auto()
    UNIVERSAL = auto()
