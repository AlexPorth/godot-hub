from enum import IntFlag, auto


class OS(IntFlag):
    NONE = 0
    ANDROID = auto()
    WINDOWS = auto()
    MAC = auto()
    LINUX = auto()
    X11 = auto()
    WEB = auto()
    APP = auto()
    LIBRARY = auto()
    HEADLESS = auto()
    SERVER = auto()
    ANDROID_APP = ANDROID | APP
    ANDROID_LIBRARY = ANDROID | LIBRARY
    LINUX_HEADLESS = LINUX | HEADLESS
    LINUX_SERVER = LINUX | SERVER
